var host = "192.168.1.88"; // HOST IP IN TILE SERVER CONFIG
var port = "8081"; //PORT IN TILE SERVER CONFIG
var provider_name = "kseb"; //PROVIDER NAME IN TILE SERVER CONFIG


var tileUrl = "http://"+host+":"+port+"/maps/"+provider_name+"/{z}/{x}/{y}.pbf";

//new functions and options

	var vectorTileStyling = {
       'lma_lines': function(properties, zoom) {
        var color='blue';
        if(properties.hasOwnProperty('colour')){
            color=properties.colour;
        }
          return {
            weight: 2,
            color: color,
            opacity: 1,
            fillColor: 'yellow',
            fill: true,
            radius: 6,
            fillOpacity: 0.7
          }
			},

       'lma_polygons': function(properties, zoom) {
        var color='blue';
        if(properties.hasOwnProperty('colour')){
            color=properties.colour;
        }
          return {
            weight: 2,
            color: color,
            opacity: 1,
            fillColor: 'yellow',
            fill: true,
            radius: 6,
            fillOpacity: 0.3
          }
			},

      'lma_points': function(properties, zoom) {
        var point_type='';
	if(properties.hasOwnProperty('switch_type'))
	  point_type=properties.switch_type;
        else if(properties.hasOwnProperty('power'))
          point_type=properties.power;
        return {
          icon: get_icon(point_type)
        }
      },

	};

	var tileOptions = {
		rendererFactory: L.canvas.tile,
		attribution: '<a href="http://www.kseb.in/">&copy; KSEB</a>,<a href="http://www.icfoss.in/">&copy; ICFOSS</a>,  <a href="http://tegola.io/">&copy; Tegola</a>, <a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap</a> contributors',
		vectorTileLayerStyles: vectorTileStyling,
		subdomains: '0123',
		key: '',
		maxZoom: 30,
		interactive: true,
		getFeatureId: function(f) {
			id= f.properties.id;
			return id;
		}
	};

    var tileLayer = L.vectorGrid.protobuf(tileUrl, tileOptions)
    .on('click', function(e) {
			var properties = e.layer.properties;
      if(properties.hasOwnProperty('name')){
        L.popup()
        .setContent(properties.name)
        .setLatLng(e.latlng)
        .openOn(map);
      }
		}).addTo(map);

    var overlays = {
      'data' : tileLayer
    };

    L.control.layers(baseLayers, overlays).addTo(map);

    function formatData(json_data){ //format the attributes of each features for displaying in popup table format
      var html='<table>';
      for(key in json_data){
        html+='<tr><b>'+key+': </b>'+json_data[key]+'<br/></tr>';
      }
    html+='</table>';
    return html;
    }

    function get_icon(icon_type){
      switch(icon_type){
        case 'pole': return new L.Icon({
            iconUrl: './dist/images/power pole.png',
            iconSize: [20, 20], // size of the icon
            });

        case 'rmu': return new L.Icon({
            iconUrl: './dist/images/rmu_switch.png',
            iconSize: [20, 20], // size of the icon
            });

        case 'ab': return new L.Icon({
            iconUrl: './dist/images/ab_switch.png',
            iconSize: [20, 20], // size of the icon
            });

        case 'transformer': return new L.Icon({
            iconUrl: './dist/images/transformer.png',
            iconSize: [20, 20], // size of the icon
            });

        case 'substation': return new L.Icon({
            iconUrl: './dist/images/substation.png',
            iconSize: [20, 20], // size of the icon
            });

        default: return new L.Icon({
            iconUrl: './dist/images/marker.png',
            iconSize: [10, 15], // size of the icon
            });
      }
    }

//print functionality using leaflet easyPrint module
var printer = L.easyPrint({
      		tileLayer: tileLayer,
      		sizeModes: ['Current', 'A4Landscape', 'A4Portrait'],
      		filename: 'myMap',
      		exportOnly: true,
      		hideControlContainer: true
		}).addTo(map);
